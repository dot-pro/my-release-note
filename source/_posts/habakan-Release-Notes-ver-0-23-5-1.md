---
title: habakan Release Notes ver 0.23.5.1
date: 2019-11-01 18:50:09
tags: habakan
---

habakan (ver 0.23.5.1)がリリースされました。更新情報は以下の通りです。

## 技術
- KaggleのObject DetectionコンペのためにCenterNetを実装しました
- React.js, React Nativeのドキュメントを読み始めました
- 論文「Enhancing Automated Malware Analysis Machines with Memory Analysis」を読みました

## 数学
- ラグランジュ未定乗数法からKKT条件への導出について勉強しました

## 本
- Measure What Matters（メジャー・ホワット・マターズ） 伝説のベンチャー投資家がGoogleに教えた成功手法 OKR
- ツナグ　想い人の心得

## その他
- じぶんリリースノートの執筆を開始しました
- Trelloで個人スクラムの運用を開始しました

