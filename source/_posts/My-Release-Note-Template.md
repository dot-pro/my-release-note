---
title: My Release Note Template
date: 2019-10-18 20:17:58
tags: info
---

Username(ver 0.0.0)がリリースされました。更新内容は以下の通りです。

## 技術・開発
- Flaskを利用してLINEBotを実装しました
- ポートフォリオをGithubに公開しました

## イベント
- Djangoの勉強会に参加しました

## 本
- [PythonによるWebスクレイピング](https://www.amazon.co.jp/Python%E3%81%AB%E3%82%88%E3%82%8BWeb%E3%82%B9%E3%82%AF%E3%83%AC%E3%82%A4%E3%83%94%E3%83%B3%E3%82%B0-Ryan-Mitchell/dp/4873117615)を読みました

## 記録

## ふりかえり
### Keep
### Problem
### Try
